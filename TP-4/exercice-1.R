# Régression
# TP-4
# NGUYEN Jeremy
# Exercice 1
################################################
library(mlbench)
library(data.table)
library(corrplot)
# Question 1
#
dataset = data.table(BostonHousing)
summary(dataset)
sapply(dataset)
dim(dataset)
cor(dataset[,-4])
corrplot(cor(dataset[,-4]), type="upper", order="hclust", tl.col="black", tl.srt=45)

lm.fit_lstat=lm(medv~lstat,data=dataset)
lm.fit_zn=lm(medv~zn,data=dataset)
lm.fit_ptratio=lm(medv~ptratio,data=dataset)
summary(lm.fit_lstat)
summary(lm.fit_zn)
summary(lm.fit_ptratio)

lm.predict_lstat = predict(lm.fit_lstat)
lm.predict_zn = predict(lm.fit_zn)
lm.predict_ptratio = predict(lm.fit_ptratio)
summary(lm.predict_lstat)
summary(lm.predict_zn)
summary(lm.predict_ptratio)

plot(dataset$medv, lm.predict_lstat, main="Pr�d. R�g. lstat", xlab="x", ylab="y")
plot(dataset$medv, lm.predict_zn, main="Pr�d. R�g. zn", xlab="x", ylab="y")
plot(dataset$medv, lm.predict_ptratio, main="Pr�d. R�g. ptratio", xlab="x", ylab="y")